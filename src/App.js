import logo from "./logo.svg";
import "./App.css";
import React from "react";

class Navbar extends React.Component {
  render() {
    return (
      <>
        <nav className="nav">
          <div>
            <h1>WIX</h1>
            <i class="fas fa-desktop"></i>
            <i class="fas fa-mobile-alt"></i>
          </div>
          <div>
            <p>Нажмите Редактировать, чтобы создать ваш сайт!</p>
            <p>
              <a href="https://github.com">Подробнее</a>
            </p>
            <button className="btn btn-primary rounded-pill">
              Редактировать
            </button>
          </div>
        </nav>
      </>
    );
  }
}

function Header() {
  return (
    <>
      <div className="header bg-dark">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="header-text">
                <p>K. Griffith</p>
                <div className="list">
                  <ul>
                    <li>
                      <a href="https://github.com">APPEARANCES</a>
                    </li>
                    <li>
                      <a href="https://github.com">BOOKS</a>
                    </li>
                    <li>
                      <a href="https://github.com">NEWS</a>
                    </li>
                    <li>
                      <a href="https://github.com">ABOUT</a>
                    </li>
                    <li>
                      <a href="https://github.com">CONTACT</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="women-img">
                <img src="/img/women.webp" />
              </div>
              <div className="text-title">
                <h1>Kayla Griffith</h1>
                <p>Award Winning Author</p>
              </div>
              <div className="head">
                <div className="header-title me-5">
                  <p className="text-p">Mew Release</p>
                  <span className="text-span"></span>
                  <p className="text-p1">The Swan Isle (2023)</p>
                  <p className="text-p2">
                    I'm a paragraph. Click here to add your own text and edit
                    me. It’s easy. Just click “Edit Text” or double click me to
                    add your own content and make changes to the font. I’m a
                    great place for you to tell a story and let your users know
                    a little more about you.
                  </p>
                  <p className="text-p3 mb-5">Order Now</p>
                  <div>
                    <button className="btn rounded-pill me-4">Amazon</button>
                    <button className="btn rounded-pill me-4">Google</button>
                    <button className="btn rounded-pill">ibooks</button>
                  </div>
                </div>
                <div className="title-img ms-5">
                  <img src="/img/swan.webp" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="text-img"></div>
    </>
  );
}

function Praise() {
  return (
    <>
      <section className="praise">
        <div className="container">
          <div className="row">
            <div className="col">
              <h1 className="text-center">Praise & Reviews</h1>
              <span className="praise-span"></span>
              <span className="praise-span1"></span>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col">
              <p className="simbol text-center"> &#10077;</p>
              <p className="text-center pe-5 ps-5 sim-text">
                I'm a paragraph. Click here to add your own text and edit me.
                It’s easy. Just click “Edit Text” or double click me to add your
                own content and make changes to the font. I’m a great place for
                you to tell a story and let your users know a little more about
                you.
              </p>
              <p className="text-center sim-text1">
                ‘Swan Isle’ is Griffith's best writing yet” The Times Book
                Review
              </p>
            </div>
            <div className="col">
              <p className="simbol text-center"> &#10077;</p>
              <p className="text-center pe-5 ps-5 sim-text">
                I'm a paragraph. Click here to add your own text and edit me.
                It’s easy. Just click “Edit Text” or double click me to add your
                own content and make changes to the font. I’m a great place for
                you to tell a story and let your users know a little more about
                you.
              </p>
              <p className="text-center sim-text1">
                ‘Swan Isle’ is Griffith's best writing yet” The Times Book
                Review
              </p>
            </div>
            <div className="col">
              <p className="simbol text-center">&#10077;</p>
              <p className="text-center pe-5 ps-5 sim-text">
                I'm a paragraph. Click here to add your own text and edit me.
                It’s easy. Just click “Edit Text” or double click me to add your
                own content and make changes to the font. I’m a great place for
                you to tell a story and let your users know a little more about
                you.
              </p>
              <p className="text-center sim-text1">
                ‘Swan Isle’ is Griffith's best writing yet” The Times Book
                Review
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

function See() {
  return (
    <>
      <section className="see">
        <div className="container">
          <div className="row mb-5">
            <div className="col">
              <p className="text-center see-text1">See Upcoming Appearances</p>
              <p className="text-center see-text2">
                I'm a paragraph. Click here to add your own text and edit me.
                It's easy.
              </p>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col">
              <div className="see-list">
                <p className="text-center see-list-text">
                  January 18th 2023, The Breakfast Club, Virtual Book Reading of
                  Swan Isle, 7PM - 8PM EST
                </p>
                <button className="btn rounded-pill see-list-btn">Join</button>
              </div>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col">
              <div className="see-list">
                <p className="text-center see-list-text">
                  January 18th 2023, The Breakfast Club, Virtual Book Reading of
                  Swan Isle, 7PM - 8PM EST
                </p>
                <button className="btn rounded-pill see-list-btn">Join</button>
              </div>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col">
              <div className="see-list">
                <p className="text-center see-list-text">
                  January 18th 2023, The Breakfast Club, Virtual Book Reading of
                  Swan Isle, 7PM - 8PM EST
                </p>
                <button className="btn rounded-pill see-list-btn">Join</button>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="about bg-dark">
                <div className="avatar">
                  <div className="avatar-h">
                  <span className="avatar-span"></span>
                  <span className="avatar-span1 bg-dark"></span>
                  </div>
                  <img src="img/avatar.webp" />
                </div>
                <p className="text-center about-text1">About Kayla Griffith</p>
                <p className="text-center about-text2">
                  I'm a paragraph. Click here to add your own text and edit me.
                  It’s easy. Just click “Edit Text” or double click me to add
                  your own content and make changes to the font. I’m a great
                  place for you to tell a story and let your users know a little
                  more about you.
                </p>
                <button className="btn rounded-pill read mt-5">
                  Read More
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer>
        <p className="text-center pt-4">
          © 2023 by K.Griffith. Proudly created with Wix.com
        </p>
      </footer>
    </>
  );
}
class App extends React.Component {
  render() {
    return (
      <>
        <Navbar />
        <Header />
        <Praise />
        <See />
      </>
    );
  }
}

export default App;
